## revision_checker.py
#### _Python3 script, which provides current branch name and revision data from remote users work directory._
### How it works ###
**Revision_checker** takes inventory file and updates it with result of checking revision. 
If in remote directory **git/svn** project exists,
script sets new key **'revision_success' = true** and additional information of project to file.
If attempt to retrieve data failes, it will set **'revision_success' = false**. 
For details, you can check **revision_checker.log**, which automatically creates in current directory.

### Requirements ###
- Python>=3.9
- requirements.txt
### How to use ###
If python3 associated with python less than 3.9:
```sh
python3.9 -m pip install -r requirements.txt
python3.9 revision_checker.py --help
usage: revision_checker.py [-h] -i INVENTORY [-l LOGLEVEL]

This script gets credentials from inventory file(json) and updates it with revision data from git/svn projects of hosts(in ~/bw dirtectory)

optional arguments:
  -h, --help            show this help message and exit
  -i INVENTORY, --inventory INVENTORY
                        Path to inventory file
  -l LOGLEVEL, --loglevel LOGLEVEL
                        Level of logging, could be DEBUG, ERROR
```
### How to test ###
This script developed and tested in Linux Mint 20.3.
Before start testing, it's needed to prepare test environment, by using **prepare_test_environment.sh** script.
There are some test-json files in **test** directory, which invoked by **Test_revision_checker.py**.
Don't hurry up, test duration is about 140 seconds.
```sh
sudo bash prepare_test_environment.sh
python3.9 Test_revision_checker.py
```
Check **valid_json.json** for result.
