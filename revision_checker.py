import my_functions


# Main function
def main():
    my_functions.logger.debug('------Script started')
    inventory_file = my_functions.args.inventory
    inventory = my_functions.get_json(inventory_file)
    for host in inventory['hosts']:
        result = dict()
        try:
            cur_user = inventory['hosts'][host]['user']
            cur_connection_addr = inventory['hosts'][host]['host']
            cur_password = cur_user
        except KeyError as err:
            my_functions.logger.error("Not valid key - \"{}\" in inventory file. Exiting...".format(err.args[0]))
            exit(3)
        
        auth_method = my_functions.check_connection(cur_connection_addr, cur_user, cur_password)['auth']
        if auth_method == 'failed':
            result.clear()
            result['revision_success'] = False
            my_functions.update_json(inventory_file, host, result)
            continue
        # Checking for directory existence
        bash_command = 'cd ~/bw'
        stdout, stderr = my_functions.remote_command(
            target_host=cur_connection_addr,
            user=cur_user,
            password=cur_password,
            auth_method=auth_method,
            command=bash_command
        )
        # If errors -> update json with key 'revision_success' = False for current host
        if stderr != "":
            my_functions.logger.error('Host: \"{}\". STDERR for command \"{}\": {}'.format(
                host, bash_command, stderr))
            result['revision_success'] = False
            my_functions.update_json(inventory_file, host, result)
            continue
        # Try to get current git branch for dir
        bash_command = 'cd ~/bw && git branch --show-current'
        stdout, stderr = my_functions.remote_command(
            target_host=cur_connection_addr,
            user=cur_user,
            password=cur_password,
            auth_method=auth_method,
            command=bash_command
        )
        # If no errors, trying to get rev-parse
        if stderr == "":
            my_functions.logger.debug('Host: \"{}\". Current git branch in ~/bw is \"{}\"'.format(host, stdout))
            result['git_current_branch'] = stdout
            bash_command = 'cd ~/bw && git rev-parse HEAD'
            stdout, stderr = my_functions.remote_command(
                target_host=cur_connection_addr,
                user=cur_user,
                password=cur_password,
                auth_method=auth_method,
                command=bash_command
            )
            # If git repo was inited, but no commits, it is possible to catch errors in this case
            if stderr != "":
                my_functions.logger.error('Host: \"{}\". STDERR for command \"{}\": {}'.format(
                    host, bash_command, stderr))
                result.clear()
                result['revision_success'] = False
                my_functions.update_json(inventory_file, host, result)
                continue
            # All correct? -> Update json with new data
            else:
                my_functions.logger.debug('Host: \"{}\". Current git rev-parse HEAD in ~/bw is \"{}\"'.format(
                    host, stdout))
                result['git_current_revision'] = stdout
                result['revision_success'] = True
                my_functions.update_json(inventory_file, host, result)
        # If there is no git repo (errors at command git branch --show-current), let's try svn
        else:
            my_functions.logger.debug('Host: \"{}\". STDERR for command \"{}\": {} Trying to check svn repo'.format(
                host, bash_command, stderr))
            bash_command = 'cd ~/bw && svn info'
            stdout, stderr = my_functions.remote_command(
                target_host=cur_connection_addr,
                user=cur_user,
                password=cur_password,
                auth_method=auth_method,
                command=bash_command
            )
            # If there is no svn repo too, update json with 'revision_success' = False
            if stderr != "":
                my_functions.logger.error('Host: \"{}\". STDERR for command \"{}\": {}'.format(
                    host, bash_command, stderr))
                result.clear()
                result['revision_success'] = False
                my_functions.update_json(inventory_file, host, result)
                continue
            # Parse output fron svn info
            else:
                result['svn_current_revision'] = my_functions.parse_svn_out(stdout)['Revision']
                result['svn_current_branch'] = my_functions.parse_svn_out(stdout)['URL']
                my_functions.logger.debug('Host: \"{}\". Current svn revision in \"~/bw\" is \"{}\"'.format(
                    host, result['svn_current_revision']))
                my_functions.logger.debug('Host: \"{}\". Current svn branch in \"~/bw\" is \"{}\"'.format(
                    host, result['svn_current_branch']))
                result['revision_success'] = True
                my_functions.update_json(inventory_file, host, result)
    my_functions.logger.debug("Script finished------")
    return 0

main()
