#!/bin/bash
set -e
users=(guest001 guest002 guest003 guest004 guest005 guest006 guest007)

check_root(){
  if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

}

prepare(){
  apt-get update && apt-get install -y subversion git
}

clean() {
  set +e
  for user in ${users[*]}; do
    userdel -r $user
  done
  set -e
}

create_user(){
  useradd -m -s /bin/bash $1
  #set password like username
  echo $1:$1 | chpasswd
}

check_root
clean
prepare
for user in ${users[*]}; do
  if [[ $user == "guest001" ]]; then
    create_user $user && mkdir -p /home/$user/svnrep && mkdir -p /home/$user/proj1 && \
    echo '123' >> /home/$user/proj1/123.txt && cd /home/$user/svnrep && svnadmin create proj1 && \
    svn import /home/$user/proj1 file:///home/$user/svnrep/proj1/trunk -m "Initial commit" && \
    mkdir /home/$user/bw && cd /home/$user/bw && svn co file:///home/$user/svnrep/proj1/trunk /home/$user/bw && \
    echo '456' >> 123.txt && svn commit -m "2nd commit" && \
    chown -R $user:$user /home/$user/svnrep && \
    chown -R $user:$user /home/$user/proj1 && \
    chown -R $user:$user /home/$user/bw
  elif [[ $user == "guest002" ]]; then
    create_user $user && mkdir /home/$user/bw && cd /home/$user/bw && \
    git init && git config user.name "$user" && git config user.email "$user@example.com" && \
    echo '123' >> 123.txt && git add . && git commit -m "Initial commit" && \
    git checkout -b devel && echo '456' >> 123.txt && git add . && git commit -m "2nd commit" && \
    chown -R $user:$user /home/$user/bw
  elif [[ $user == "guest003" ]]; then
    create_user $user && mkdir /home/$user/bw && chown -R $user:$user /home/$user/bw
  elif [[ $user == "guest004" ]]; then
    create_user $user
  elif [[ $user == "guest005" ]]; then
    echo "Nothing do for $user"
  elif [[ $user == "guest006" ]]; then
    echo "Nothing do for $user"
  elif [[ $user == "guest007" ]]; then
    create_user $user && echo $user:wrong_pass | chpasswd
  fi
done

