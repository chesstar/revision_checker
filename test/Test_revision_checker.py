import unittest
import subprocess


class MyTestCase(unittest.TestCase):

    def test_something(self):
        complete1 = subprocess.run(['python3', '../revision_checker.py', '-i', 'invalid_json.json'])
        self.assertEqual(complete1.returncode, 1)
        complete2 = subprocess.run(['python3', '../revision_checker.py', '-i', 'valid_json_invalid_key.json'])
        self.assertEqual(complete2.returncode, 2)
        complete0 = subprocess.run(['python3', '../revision_checker.py', '-i', 'valid_json.json'])
        self.assertEqual(complete0.returncode, 0)


if __name__ == '__main__':
    unittest.main()
