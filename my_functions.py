import paramiko
import json
import logging.handlers
import os
from argparse import ArgumentParser
# Block of additional functions

private_key_file = os.path.expanduser('~/.ssh/id_rsa')
my_key = paramiko.RSAKey.from_private_key_file(private_key_file)


def get_json(path):
    try:
        with open(path, 'r') as f:
            data = json.load(f)
    except Exception as err:
        logger.error("Error with input file. Class: {}; Description: {}".format(type(err), err.args))
        exit(1)
    try:
        data['hosts']
    except KeyError:
        logger.error("Not valid inventory file. Exiting...")
        exit(2)
    return data


def update_json(json_file, host, res):
    cur_dict = get_json(json_file)
    for k in res:
        cur_dict['hosts'][host][k] = res[k]
    with open(json_file, 'w') as f:
        f.write(json.dumps(cur_dict, indent=4))
    return 0


def parse_svn_out(output):
    parsed = dict()
    strings = output.split('\n')
    for s in strings:
        if s != "":
            cur_str = s.split(': ')
            parsed[cur_str[0]] = cur_str[1]
    return parsed


def check_connection(target_host, user, password):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # Try to connect with pubkey
    try:
        client.connect(hostname=target_host, username=user, pkey=my_key, port=22)
        if client.get_transport() is not None:
            if client.get_transport().is_active():
                client.close()
                logger.debug('Successfully authenticated with pubkey for host: \"{}\"'.format(target_host))
                return {'auth': 'pubkey'}
    except paramiko.ssh_exception.AuthenticationException:
        logger.debug("Auth for host \"{}\" with pubkey failed, trying auth with password...".format(target_host))
        # If auth with pubkey failed, try to auth with password
        try:
            client.connect(hostname=target_host, username=user, password=password, port=22)
            if client.get_transport() is not None:
                if client.get_transport().is_active():
                    client.close()
                    logger.debug('Successfully authenticated with password for host: \"{}\"'.format(target_host))
                    return {'auth': 'password'}
        except paramiko.ssh_exception.AuthenticationException as err:
            logger.error('Host \"{}\". Paramiko exception: {}(Wrong credentials?)'.format(
                target_host, err.args[0]))
    # Catch other errors like timeout, no resolve, etc...
    except Exception as err:
        logger.error(
            "Host - \"{}\" doesn't response. "
            "Exception instance: {}"
            "Exception description: {}"
            "Next host...".format(target_host, type(err), err.args))
    return {'auth': 'failed'}


def remote_command(target_host, user, password, auth_method, command):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if auth_method == 'pubkey':
        client.connect(hostname=target_host, username=user, pkey=my_key, port=22)
    elif auth_method == 'password':
        client.connect(hostname=target_host, username=user, password=password, port=22)
    stdin, stdout, stderr = client.exec_command(command)
    str_out = stdout.read().decode('utf8').strip('\n')
    str_err = stderr.read().decode('utf8').strip('\n')
    client.close()
    logger.debug("Close connect for host: \"{}\", after executed command: \"{}\"".format(target_host, command))
    return (str_out, str_err)


# Init parser
parser = ArgumentParser(
    description='''
    This script gets credentials from inventory file(json)
    and updates it with revision data from git/svn projects of hosts(in ~/bw dirtectory)
    '''
)
parser.add_argument('-i', '--inventory',
                    required=True,
                    help='Path to inventory file')
parser.add_argument('-l', '--loglevel',
                    required=False,
                    default='ERROR',
                    help='Level of logging, could be DEBUG, ERROR')
args = parser.parse_args()

# Init logger
log_levels = {'DEBUG': logging.DEBUG, 'ERROR': logging.ERROR}
progname = 'revision_checker'
logger = logging.getLogger(progname)
logger.setLevel(log_levels.get(args.loglevel.upper(), logging.ERROR))
handler = logging.handlers.RotatingFileHandler('./{}.log'.format(progname), maxBytes=1000000, backupCount=8)
formatter = logging.Formatter('%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)